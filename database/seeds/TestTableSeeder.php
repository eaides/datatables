<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as faker;
use dttables89\Test;

class TestTableSeeder extends Seeder {

    public function run()
    {
        /* If just exists the user admin, create $create fakes users, expect to create more or less 25% as
         * db_admin and 75% as user. Expect to create 50% of them as active and other 50% as not
         * active yet with activation code and valid_until date time of 30 days from now.
         */
        $create = 50000;
        if (\DB::table('test')->count()<1) {
            $faker = Faker::create();
            for($i=0; $i<$create; $i++) {
                $active = rand(0,1);    // 50% active, 50% not active
                $code = '';
                $valid_until = '0000-00-00 00:00:00';
                if ($active==0) {
                    $code = str_random(60);
                    $valid_until = new DateTime("+30 days");
                }
                $test = new Test;
                $test->first    = $faker->firstName;
                $test->second   = $faker->lastName;
                $test->third    = $faker->firstName . ' ' . $faker->lastName;
                $test->save();
            }
        }
    }

}