<?php namespace dttables89\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        /*
		* Route excluded for Ajax request
		*/
        $except_routes = array(
            'test/data',
        );
        /*
        * exclude routes from CSFR token verification
        */
        foreach ($except_routes as $key => $route) {
            if($request->is($route)){
                return parent::addCookieToResponse($request, $next($request));
            }
        }
		return parent::handle($request, $next);
	}

}
