<?php

use dttables89\Test;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');
//
//Route::get('home', 'HomeController@index');
//
//Route::controllers([
//	'auth' => 'Auth\AuthController',
//	'password' => 'Auth\PasswordController',
//]);

Route::get('/', array('as' => 'home-page', function()
{
    return view('datatables');
}));

Route::get('agent', array('as' => 'agent-page', function()
{
    return Agent::platform();
}));

Route::get('test/data',function(){

    // the problem with the OR, need to group into () all the ORs
    //    $test = \dttables89\Test::select('*')
    //        ->Where('First','like','a%')
    //        ->orWhere('First','like','e%')
    //        ->orWhere('First','like','i%')
    //        ->orWhere('First','like','o%')
    //        ->orWhere('First','like','u%');
    //    select * from `test` where `First` like '%a%' or `First` like '%e%' or `First` like '%i%'
    //      or `First` like '%o%' or `First` like '%u%' and
    //      (LOWER(first) LIKE '%aides%' or LOWER(second) LIKE '%aides%'
    //      or LOWER(third) LIKE '%aides%')
    //      order by `first` asc limit 10 offset 20

    // like this:
    $test = Test::select('*')
        ->Where(function($test) {
            $test->Where('First', 'like', 'a%')
                ->orWhere('First', 'like', 'e%')
                ->orWhere('First', 'like', 'i%')
                ->orWhere('First', 'like', 'o%')
                ->orWhere('First', 'like', 'u%');
            });
    // select * from `test` where
    //      (`First` like '%a%' or `First` like '%e%' or `First` like '%i%'
    //          or `First` like '%o%' or `First` like '%u%')
    //          and (LOWER(first) LIKE '%aide%' or LOWER(second) LIKE '%aide%'
    //          or LOWER(third) LIKE '%aide%')
    //          order by `first` asc
    //          limit 10 offset 30

    return \Datatables::of($test)
        ->addColumn('actions', 'action here')
        ->make(true);
});

Route::get('/user/login', array('as' => 'user-login', function() {
}));
Route::get('/user/create', array('as' => 'user-create', function() {
}));

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/test', function() {
   Test::all();
});