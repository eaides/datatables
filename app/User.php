<?php namespace dttables89;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * dttables89\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\dttables89\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\User whereUpdatedAt($value)
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

}
