<?php namespace dttables89;

use Illuminate\Database\Eloquent\Model;

/**
 * dttables89\Test
 *
 * @property integer $id
 * @property string $first
 * @property string $second
 * @property string $third
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\dttables89\Test whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\Test whereFirst($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\Test whereSecond($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\Test whereThird($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\Test whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\dttables89\Test whereUpdatedAt($value)
 */

class Test extends Model {

    protected $table = 'test';

    protected $fillable = ['name', 'email', 'password'];

}
