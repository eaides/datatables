<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Site Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

    'contact_us' => 'צור קשר',
    'sign_up' => 'הרשמה',
    'login' => 'התחבר',
    'copy_right' => 'estudio89 Software',
    'logged_in' => 'מחובר כ',
    'logout' => 'התנתק',
    'home' => 'בית',
    'home_page' => 'עמוד הבית',
    'captcha' => 'Captcha',
    'captcha_msg' => 'מילת אימות',
    'min4' => '(מינימום 4 תווים)',

);
