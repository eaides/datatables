<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Site Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'contact_us' => 'Fale Conosco',
    'sign_up'    => 'Cadastre-se',
    'login' => 'Entrar',
    'copy_right' => 'estudio89 Software',
    'logged_in' => 'Logado como',
    'logout' => 'Sair',
    'home' => 'Principal',
    'home_page' => 'Página Principal',
    'captcha' => 'Captcha',
    'captcha_msg' => 'Verificação da palavra',
    'min4' => '(Mínimo de 4 caracteres)',

);
