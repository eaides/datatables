<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Site Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'contact_us' => 'Contáctenos',
    'sign_up' 	 => 'Regístrese',
    'login' => 'Iniciar Sesión',
    'copy_right' => 'estudio89 Software',
    'logged_in' => 'Conectado como',
    'logout' => 'Salir',
    'home' => 'Inicio',
    'home_page' => 'Página de Inicio',
    'captcha' => 'Captcha',
    'captcha_msg' => 'Verificación de Palabra',
    'min4' => '(mínimo 4 carácteres)',

);
