<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| User Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'register'              => 'Registrar',
	'login'                 => 'Iniciar Sesión',
	'login_first'           => 'Iniciar Sesión Primero',
	'account'               => 'Cuenta',
	'forgot_password'       => '¿ Olvidaste tu Contraseña ?',
	'settings'              => 'Configuración',
	'profile'               => 'Perfil',
	'user_account_is_not_confirmed' => 'La cuenta de usuario no está confirmada.',
	'user_account_updated'          => 'Cuenta de usuario actualizada.',
	'user_account_created'          => 'Cuenta de usuario creada.',

);