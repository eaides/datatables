<?php

return array(

	'first_name' => 'First Name',
	'last_name'  => 'Last Name',
	'user_id'  => 'User Id',
	'username'  => 'Username',
	'email'      => 'Email',
	'groups'     => 'Groups',
	'roles'     => 'Roles',
	'activated'  => 'Activated',
	'created_at' => 'Created at',
    'username_or_email' => 'Username or E-Mail',
    'name' => 'Name',

    'user_active_values' => [
        '_' => 'All',
        '1' => 'Active',
        '0' => 'Inactive',
    ],
);
