<?php

return array(

	'yes' => 'Yes',
	'no'  => 'No',
    'must_login' => 'Must be logged in.',

    'string_test' => 'english test string for translation',
    'string_test2' => 'english second test string for translation',
);
