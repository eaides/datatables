<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Site Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'contact_us' => 'Contact Us',
    'sign_up' => 'Sign Up',
    'login' => 'Login',
    'copy_right' => 'estudio89 Software',
    'logged_in' => 'Logged in as',
    'logout' => 'Logout',
    'home' => 'Home',
    'home_page' => 'Home Page',
    'captcha' => 'Captcha',
    'captcha_msg' => 'Verification Word',
    'min4' => '(minimum 6 characters)',
    'search' => 'Search',
    'all' => 'All',
);
