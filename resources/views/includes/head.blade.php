<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>
    @section('page_title')
        Default Title
    @show
</title>

<!-- All combined CSS -->
{!! HTML::style("css/bootstrap.min.css") !!}
{!! HTML::style("css/font-awesome.min.css") !!}
{!! HTML::style("css/jquery.dataTables.min.css") !!}

@yield('custom_styles')

<!-- Fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    {!! HTML::script("js/html5shiv.min.js") !!}
    {!! HTML::script("js/respond.min.js") !!}
<![endif]-->