<div class="navbar navbar-default navbar-fixed-bottom" id="copyright text-right">
    <div class="container">
        <p class="navbar-text text-center" style="width: 100%;"><i class="fa fa-copyright"></i> Copyright <?php $start = 2014; echo (date('Y')>$start) ? $start."-".date("Y") : $start; ?> Estudio '89 Software</p>
    </div>
</div>

