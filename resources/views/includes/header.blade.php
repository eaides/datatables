<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li {{ (URL::route('home-page') === URL::current() ? ' class="active"' : '') }} ><a href="{{ URL::route('home-page') }}"> {{{ Lang::get('site.home') }}} </a></li>
        <li ><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::check())
            @if (Auth::user()->hasRole('admin'))
                <li>
                    <a href="#">Admin Panel</a>
                </li>
            @endif
            <li>
                <a href="#"> {{{ Lang::get('site.logged_in') }}} {{{ Auth::user()->name }}}</a>
            </li>
            <li>
                <a href="{{{ URL::route('user-logout') }}}">{{{ Lang::get('site.logout') }}}</a>
            </li>
        @else
            <li {{ (URL::route('user-login') === URL::current() ? ' class="active"' : '') }}>
                <a href="{{{ URL::route('user-login') }}}">{{{ Lang::get('site.login') }}}</a>
            </li>
            <li {{ (URL::route('user-create') === URL::current() ? ' class="active"' : '') }}>
                <a href="{{{ URL::route('user-create') }}}">{{{ Lang::get('site.sign_up') }}}</a>
            </li>
        @endif
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>