    <!-- Custom Javascript -->
    {!! HTML::script("js/jquery.min.js") !!}
    {!! HTML::script("js/bootstrap.min.js") !!}
    {!! HTML::script("js/jquery.dataTables.min.js") !!}
    <script>
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <!-- Custom Page JavaScript -->
    @yield('custom_js')
