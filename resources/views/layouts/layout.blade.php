<!DOCTYPE html>
<html lang="en">

@if (!isset($extra_body_class))
    <?php $extra_body_class = ''; ?>
@endif
@if(App::getLocale()!='he')
    <?php $dir = 'LTR'; $extra_body_class .= ' ltr'; ?>
@else
    <?php $dir = 'RTL'; $extra_body_class .= ' rtl'; ?>
@endif

<head>
    {{-- @include('includes.head') --}}
    @include('includes.head', array('dir' => $dir))
</head>

<body dir="{{{ $dir }}}" class="{{{ $extra_body_class }}}">

    <div class="GroupButtonsOpenBootstrapModals" style="display:none">
        <!-- Button trigger bootstrap main modal 1 -->
        <button id="OpenBootstrapMainModal-1" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#BootstrapMainModal-1">
        <i class="fa fa-caret-square-o-down"></i>
        </button>

        <!-- Button trigger bootstrap main modal 2 -->
        <button id="OpenBootstrapMainModal-2" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#BootstrapMainModal-2">
        <i class="fa fa-caret-square-o-down"></i>
        </button>

        <!-- Button trigger bootstrap main modal 3 -->
        <button id="OpenBootstrapMainModal-2" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#BootstrapMainModal-3">
        <i class="fa fa-caret-square-o-down"></i>
        </button>
    </div>

    <!-- Bootstrap Modal-1 -->
    <div class="modal fade" id="BootstrapMainModal-1" tabindex="-1" role="dialog" aria-labelledby="BootstrapMainModal-1-Label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="BootstrapMainModal-1-Label">
                    @section('modal-1-title')
                    Modal 1
                    @show
                    </h4>
                </div>
            <div class="modal-body">
                @section('modal-1-body')
                ...
                @show
            </div>
            <div class="modal-footer">
                @section('modal-1-footer')
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                @show
            </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap Main Modal-2 -->
    <div class="modal fade" id="BootstrapMainModal-2" tabindex="-1" role="dialog" aria-labelledby="BootstrapMainModal-2-Label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="BootstrapMainModal-2-Label">
                    @section('modal-2-title')
                    Modal 2
                    @show
                    </h4>
                </div>
            <div class="modal-body">
                @section('modal-2-body')
                ...
                @show
            </div>
            <div class="modal-footer">
                @section('modal-2-footer')
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                @show
            </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap Main Modal-3 -->
    <div class="modal fade" id="BootstrapMainModal-3" tabindex="-1" role="dialog" aria-labelledby="BootstrapMainModal-3-Label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="BootstrapMainModal-3-Label">
                    @section('modal-3-title')
                    Modal 3
                    @show
                    </h4>
                </div>
            <div class="modal-body">
                @section('modal-3-body')
                ...
                @show
            </div>
            <div class="modal-footer">
                @section('modal-3-footer')
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                @show
            </div>
            </div>
        </div>
    </div>

    <div id="wrapper">

        <header class="row">
            @include('includes.header')
        </header>

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('message'))
                        <div class="flash alert">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    @if(Session::has('flash_notice'))
                        <div id="flash_notice">{{ Session::get('flash_notice') }}</div>
                    @endif
                    @if (Session::has('flash_error'))
                        <div id="flash_error">{{ Session::get('flash_error') }}</div>
                    @endif

                    {{-- use 'main' as scaffolding --}}
                    @yield('main')
                    {{-- or use 'content' as default --}}
                    @yield('content')

                </div>
            </div>
        </div>

        <footer class="row">
            @include('includes.footer')
        </footer>
    </div>

    @include('includes.foot')

</body>

</html>

