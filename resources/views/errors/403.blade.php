<!DOCTYPE html>
<html lang="en">

<head>

    @section('page_title')
        Forbbiden - 403
    @stop
    @include('includes.head')

</head>

<body>
    <br><br>
    <div class="container">
        <div class="jumbotron text-center">
            <h1><i class="fa fa-ban fa-lg text-danger"></i> Forbidden <small class="text-danger">Error 403</small></h1>
            <br />
            <p>Sorry you are not authorized to visit this link.</p>
            <p>Use your browsers <b>Back</b> button to navigate to the page you have previously come from</p>
            <p><b>Or you could just press this neat little button:</b></p>
            <a href="{{ URL::route('home-page') }}" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;Home&nbsp;&nbsp;&nbsp;</a>
        </div>
    </div>

</body>

