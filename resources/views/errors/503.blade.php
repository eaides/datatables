<!DOCTYPE html>
<html lang="en">

<head>

    @section('page_title')
        Server is Down - 503
    @stop
    @include('includes.head')

</head>

<body>
    <br><br>
    <div class="container">
        <div class="jumbotron text-center">
            <h1><i class="fa fa-cogs fa-lg text-info"></i> Server is Down <small class="text-info">Error 503</small></h1>
            <br />
            <p>Sorry the server is Down for Maintenance.</p>
            <p>Try to reload the page in some minutes</p>
            <p><b>Or you could just press this neat little button from time to time till the server will be available:</b></p>
            <a href="{{ URL::route('home-page') }}" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;Home&nbsp;&nbsp;&nbsp;</a>
        </div>
    </div>

</body>

