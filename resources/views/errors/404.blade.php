<!DOCTYPE html>
<html lang="en">

<head>

    @section('page_title')
        Page Not Found - 404
    @stop
    @include('includes.head')

</head>

<body>
    <br><br>
    <div class="container">
        <div class="jumbotron text-center">
            <h1><i class="fa fa-exclamation-triangle fa-lg text-warning"></i> Page Not Found <small class="text-warning">Error 404</small></h1>
            <br />
            <p>The page you requested could not be found, either contact your webmaster or try again.</p>
            <p>Use your browsers <b>Back</b> button to navigate to the page you have previously come from</p>
            <p><b>Or you could just press this neat little button:</b></p>
            <a href="{{ URL::route('home-page') }}" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;Home&nbsp;&nbsp;&nbsp;</a>
        </div>
    </div>


</body>
