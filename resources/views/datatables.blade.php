@extends('layouts.layout')

@section('content')
    <h3>Datatables</h3><hr/>
    <table id="dttest" class="table table-hover table-condensed">
    {{--        <thead>
            <tr>
                <th class="col-md-3">First</th>
                <th class="col-md-3">Second</th>
                <th class="col-md-3">Third</th>
                <th class="col-md-3">Actions</th>
            </tr>
            </thead>
        </table> --}}
    @stop

    @section('custom_js')
        <script type="text/javascript">
            jQuery(document).ready(function() {
                var $table = $('#dttest').dataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    ajax: {
                        url: "test/data",
                        type: "GET"
                    },
                    columns: [
                        {
                            title: 'My First',
                            data: 'first',
                            name: 'first'
                        },
                        {
                            title: 'My Second',
                            data: 'second',
                            name: 'second'
                        },
                        {
                            title: 'My Third',
                            data: 'third',
                            name: 'third'
                        },
                        {
                            title: 'The Actions',
                            data: 'actions',
                            name: 'actions',
                            searchable: false,
                            orderable: false }
                    ]
                });
            });
        </script>
    @stop